from django.contrib import admin

from django.conf import settings
from django.urls import include, path
from django.urls import re_path

urlpatterns = [
    re_path(r"", include("user_sessions.urls", "user_sessions")),
    path("admin/", admin.site.urls),
    re_path(r"^watchman/", include("watchman.urls")),
]

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
